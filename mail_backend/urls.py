from django.urls import path, include
from rest_framework.routers import DefaultRouter

from mail_storage.views import MailSentViewSet, MailInboxViewSet

router = DefaultRouter()
router.register(r'sent', MailSentViewSet)
router.register(r'inbox', MailInboxViewSet)

urlpatterns = [
    path('api-auth/', include('rest_framework.urls'))
] + router.urls
