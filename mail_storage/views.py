from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from mail_storage.models import MailRecipient, Mail
from mail_storage.serializers import MailInboxSerializer, MailSentSerializer, MailCreateSerializer


class MailInboxViewSet(mixins.RetrieveModelMixin,
                       mixins.DestroyModelMixin,
                       mixins.ListModelMixin,
                       GenericViewSet):
    queryset = MailRecipient.objects.all()
    serializer_class = MailInboxSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(recipient=self.request.user, deleted_by_recipient=False).select_related('mail', 'mail__sender')\
            .order_by('-mail__created')
        return qs

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), mail__pk=self.kwargs['pk'])
        return obj

    def perform_destroy(self, instance):
        instance.deleted_by_recipient = True
        instance.save()

    @action(detail=True, methods=['post'], url_path='mark-as-read')
    def mark_as_read(self, request, pk):
        result = self.queryset.filter(recipient=request.user, deleted_by_recipient=False, mail__pk=pk)\
            .update(is_read=True)
        if result:
            return Response(status=status.HTTP_200_OK)
        else:
            return Response({'detail': 'Не найдено.'}, status=status.HTTP_404_NOT_FOUND)


class MailSentViewSet(mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.ListModelMixin,
                      GenericViewSet):
    queryset = Mail.objects.all()
    serializer_class = MailSentSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.action == 'create':
            return MailCreateSerializer
        else:
            return super().get_serializer_class()

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(sender=self.request.user, deleted_by_sender=False).prefetch_related('recipients')\
            .select_related('sender').order_by('-created')
        return qs

    def perform_create(self, serializer):
        serializer.save(sender=self.request.user)

    def perform_destroy(self, instance):
        instance.deleted_by_sender = True
        instance.save()
