from django.core.validators import EmailValidator
from rest_framework import serializers

from mail_storage.models import MailRecipient, Mail
from user.models import User


class MailCreateSerializer(serializers.ModelSerializer):
    to = serializers.ListField(required=True, write_only=True)

    def validate_to(self, value):
        email_validator = EmailValidator(message='Один или несколько email введены неправильно')
        for email in value:
            email_validator(email)
        return value

    def create(self, validated_data):
        recipients = validated_data.pop('to', [])
        mail = Mail.objects.create(**validated_data)
        db_recipients = []
        for recipient in recipients:
            try:
                user = User.objects.get(email=recipient)
                db_recipients.append(user)
            except User.DoesNotExist:
                pass
        mail.recipients.set(db_recipients)
        return mail

    class Meta:
        model = Mail
        fields = ('id', 'to', 'title', 'text', 'created')


class MailInboxSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='mail.pk')
    sender = serializers.EmailField(source='mail.sender.email')
    title = serializers.CharField(source='mail.title')
    text = serializers.CharField(source='mail.text')
    created = serializers.DateTimeField(source='mail.created')

    def to_representation(self, instance):
        repr_data = super().to_representation(instance)
        repr_data['from'] = repr_data['sender']
        repr_data.pop('sender')
        return repr_data

    class Meta:
        model = MailRecipient
        fields = ('id', 'sender', 'title', 'text', 'created', 'is_read')


class MailSentSerializer(serializers.ModelSerializer):
    sender = serializers.EmailField(source='sender.email')
    to = serializers.SerializerMethodField()

    def get_to(self, obj):
        return obj.recipients.all().values_list('email', flat=True)

    def to_representation(self, instance):
        repr_data = super().to_representation(instance)
        repr_data['from'] = repr_data['sender']
        repr_data.pop('sender')
        return repr_data

    class Meta:
        model = Mail
        fields = ('id', 'sender', 'to', 'title', 'text', 'created')
