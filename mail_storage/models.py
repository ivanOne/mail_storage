from django.db import models

from user.models import User


class Mail(models.Model):
    title = models.CharField('Заголовок письма', max_length=100)
    text = models.TextField('Текст письма')

    sender = models.ForeignKey(User, verbose_name='Sender', on_delete=models.CASCADE, related_name='send_mails')

    recipients = models.ManyToManyField(User, through='MailRecipient')

    deleted_by_sender = models.BooleanField('Удалено отправителем', default=False)
    created = models.DateTimeField('Создан', auto_now_add=True)

    class Meta:
        verbose_name = 'Письмо'
        verbose_name_plural = 'Письма'


class MailRecipient(models.Model):
    mail = models.ForeignKey(Mail, on_delete=models.CASCADE)
    recipient = models.ForeignKey(User, on_delete=models.CASCADE)

    is_read = models.BooleanField('Прочитано', default=False)
    deleted_by_recipient = models.BooleanField('Удалено получателем', default=False)

    class Meta:
        verbose_name = 'Получатель сообщения'
        verbose_name_plural = 'Получатели сообщений'
