from datetime import datetime
from unittest.mock import patch

from django.test import TestCase
from django.utils.timezone import make_aware
from rest_framework import status
from rest_framework.test import APIClient

from mail_storage.models import Mail, MailRecipient
from user.models import User


def mock_now():
    naive_datetime = datetime(day=19, month=9, year=2019, hour=10, minute=40, second=10)
    return make_aware(naive_datetime)


class ApiTestCase(TestCase):
    fixtures = ['users.json']
    client_class = APIClient

    def assert_sent_mail_data(self, response_data, mail):
        self.assertEqual(len(response_data.keys()), 6)
        self.assertEqual(response_data['id'], mail.id)
        self.assertEqual(response_data['from'], mail.sender.email)
        self.assertEqual(response_data['to'], list(mail.recipients.all().values_list('email', flat=True)))
        self.assertEqual(response_data['title'], mail.title)
        self.assertEqual(response_data['text'], mail.text)
        self.assertEqual(response_data['created'], mail.created.strftime("%Y-%m-%dT%H:%M:%S.%fZ"))

    def assert_inbox_mail_data(self, response_data, mail_recipient):
        self.assertEqual(len(response_data.keys()), 6)
        self.assertEqual(response_data['id'], mail_recipient.mail.id)
        self.assertEqual(response_data['from'], mail_recipient.mail.sender.email)
        self.assertEqual(response_data['title'], mail_recipient.mail.title)
        self.assertEqual(response_data['text'], mail_recipient.mail.text)
        self.assertEqual(response_data['created'], mail_recipient.mail.created.strftime("%Y-%m-%dT%H:%M:%S.%fZ"))
        self.assertEqual(response_data['is_read'], mail_recipient.is_read)


@patch('django.utils.timezone.now', mock_now)
class CreateMailTestCase(ApiTestCase):
    path = '/sent/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')
        self.recipient_3 = User.objects.get(email='user4@email.com')
        self.created_datetime_string = mock_now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        self.mail_title = 'Mail title'
        self.mail_text = 'Mail text'

    def test_success(self):
        self.client.force_login(self.sender)
        to = [self.recipient_1.email]
        response = self.client.post(self.path, {'title': self.mail_title, 'text': self.mail_text, 'to': to})
        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_data['title'], self.mail_title)
        self.assertEqual(response_data['text'], self.mail_text)
        self.assertEqual(response_data['created'], self.created_datetime_string)

        mail = Mail.objects.first()

        self.assertEqual(mail.sender, self.sender)
        self.assertEqual(mail.title, self.mail_title)
        self.assertEqual(mail.text, self.mail_text)
        self.assertFalse(mail.deleted_by_sender)

        recipients = MailRecipient.objects.filter(mail=mail)
        self.assertEqual(recipients.count(), 1)
        self.assertEqual(recipients[0].recipient, self.recipient_1)
        self.assertFalse(recipients[0].is_read)
        self.assertFalse(recipients[0].deleted_by_recipient)

    def test_multiple_recipients(self):
        self.client.force_login(self.sender)
        to = [self.recipient_1.email, self.recipient_2.email]
        response = self.client.post(self.path, {'title': self.mail_title, 'text': self.mail_text, 'to': to})
        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_data['title'], self.mail_title)
        self.assertEqual(response_data['text'], self.mail_text)
        self.assertEqual(response_data['created'], self.created_datetime_string)

        mail = Mail.objects.first()

        self.assertEqual(mail.sender, self.sender)
        self.assertEqual(mail.title, self.mail_title)
        self.assertEqual(mail.text, self.mail_text)
        self.assertFalse(mail.deleted_by_sender)
        self.assertEqual(mail.recipients.all().count(), 2)

    def test_invalid_email_in_to_field(self):
        self.client.force_login(self.sender)
        to = ['badmail@']
        response = self.client.post(self.path, {'title': self.mail_title, 'text': self.mail_text, 'to': to})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {'to': ['Один или несколько email введены неправильно']})
        self.assertEqual(Mail.objects.all().count(), 0)

    def test_recipient_user_not_exist(self):
        self.client.force_login(self.sender)
        to = [self.recipient_1.email, 'not_exist@mail.com']
        response = self.client.post(self.path, {'title': self.mail_title, 'text': self.mail_text, 'to': to})
        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_data['title'], self.mail_title)
        self.assertEqual(response_data['text'], self.mail_text)
        self.assertEqual(response_data['created'], self.created_datetime_string)

        mail = Mail.objects.first()

        self.assertEqual(mail.sender, self.sender)
        self.assertEqual(mail.title, self.mail_title)
        self.assertEqual(mail.text, self.mail_text)
        self.assertFalse(mail.deleted_by_sender)

        recipients = MailRecipient.objects.filter(mail=mail)
        self.assertEqual(recipients.count(), 1)
        self.assertEqual(recipients[0].recipient, self.recipient_1)
        self.assertFalse(recipients[0].is_read)
        self.assertFalse(recipients[0].deleted_by_recipient)

    def test_required_fields(self):
        self.client.force_login(self.sender)
        response = self.client.post(self.path)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {
            'to': ['Это поле обязательно.'],
            'title': ['Это поле обязательно.'],
            'text': ['Это поле обязательно.']
        })
        self.assertEqual(Mail.objects.all().count(), 0)

    def test_without_login(self):
        to = [self.recipient_1.email]
        response = self.client.post(self.path, {'title': self.mail_title, 'text': self.mail_text, 'to': to})

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})


class SentListTestCase(ApiTestCase):
    path = '/sent/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')
        self.recipient_3 = User.objects.get(email='user4@email.com')

        self.mail_1 = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail_1.recipients.set([self.recipient_1])

        self.mail_2 = Mail.objects.create(title='title_2', text='text_2', sender=self.sender)
        self.mail_2.recipients.set([self.recipient_1, self.recipient_2])

        self.mail_3 = Mail.objects.create(title='title_3', text='text_3', sender=self.sender)
        self.mail_3.recipients.set([self.recipient_1, self.recipient_2, self.recipient_3])

        self.mail_4 = Mail.objects.create(title='title_4', text='text_4', sender=self.recipient_1)
        self.mail_4.recipients.set([self.sender])

        self.mail_5 = Mail.objects.create(title='title_5', text='text_5', sender=self.recipient_1)
        self.mail_5.recipients.set([self.sender, self.recipient_2])

    def test_success(self):
        self.client.force_login(self.sender)
        response = self.client.get(self.path)

        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 3)
        self.assert_sent_mail_data(response_data[0], self.mail_3)
        self.assert_sent_mail_data(response_data[1], self.mail_2)
        self.assert_sent_mail_data(response_data[2], self.mail_1)

    def test_other_user(self):
        self.client.force_login(self.recipient_1)
        response = self.client.get(self.path)

        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 2)
        self.assert_sent_mail_data(response_data[0], self.mail_5)
        self.assert_sent_mail_data(response_data[1], self.mail_4)

    def test_exclude_delete_mails(self):
        self.mail_2.deleted_by_sender = True
        self.mail_2.save()

        self.client.force_login(self.sender)
        response = self.client.get(self.path)

        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 2)
        self.assert_sent_mail_data(response_data[0], self.mail_3)
        self.assert_sent_mail_data(response_data[1], self.mail_1)

    def test_without_auth(self):
        response = self.client.get(self.path)

        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response_data, {'detail': 'Учетные данные не были предоставлены.'})


class DeleteSentMailTestCase(ApiTestCase):
    path = '/sent/{}/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')

        self.mail = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail.recipients.set([self.recipient_1])

    def test_success(self):
        self.client.force_login(self.sender)
        response = self.client.delete(self.path.format(self.mail.pk))

        self.mail.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(self.mail.deleted_by_sender)

    def test_mail_other_user(self):
        self.client.force_login(self.recipient_1)
        response = self.client.delete(self.path.format(self.mail.pk))

        self.mail.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(self.mail.deleted_by_sender)

    def test_without_auth(self):
        response = self.client.delete(self.path.format(self.mail.pk))

        self.mail.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertFalse(self.mail.deleted_by_sender)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})

    def test_mail_not_exist(self):
        deleted_pk = self.mail.pk
        self.mail.delete()
        self.client.force_login(self.sender)
        response = self.client.delete(self.path.format(deleted_pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})


class DetailSentMailTestCase(ApiTestCase):
    path = '/sent/{}/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')

        self.mail = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail.recipients.set([self.recipient_1])

    def test_success(self):
        self.client.force_login(self.sender)
        response = self.client.get(self.path.format(self.mail.pk))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_sent_mail_data(response.json(), self.mail)

    def test_mail_other_user(self):
        self.client.force_login(self.recipient_1)
        response = self.client.get(self.path.format(self.mail.pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})

    def test_without_auth(self):
        response = self.client.get(self.path.format(self.mail.pk))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})

    def test_mail_not_exist(self):
        self.client.force_login(self.sender)
        deleted_pk = self.mail.pk
        self.mail.delete()
        response = self.client.get(self.path.format(deleted_pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})


class InboxMailListTestCase(ApiTestCase):
    path = '/inbox/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')

        self.mail_1 = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail_1.recipients.set([self.recipient_1])

        self.mail_2 = Mail.objects.create(title='title_2', text='text_2', sender=self.sender)
        self.mail_2.recipients.set([self.recipient_2])

        self.mail_3 = Mail.objects.create(title='title_3', text='text_3', sender=self.sender)
        self.mail_3.recipients.set([self.recipient_1, self.recipient_2])

        self.mail_4 = Mail.objects.create(title='title_4', text='text_4', sender=self.recipient_1)
        self.mail_4.recipients.set([self.sender])

    def test_success(self):
        self.client.force_login(self.recipient_1)
        response = self.client.get(self.path)

        response_data = response.json()

        recipient_emails = MailRecipient.objects.filter(recipient=self.recipient_1).order_by('-mail__created')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 2)
        self.assert_inbox_mail_data(response_data[0], recipient_emails[0])
        self.assert_inbox_mail_data(response_data[1], recipient_emails[1])

    def test_other_user(self):
        self.client.force_login(self.recipient_2)
        response = self.client.get(self.path)

        response_data = response.json()

        recipient_emails = MailRecipient.objects.filter(recipient=self.recipient_2).order_by('-mail__created')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 2)
        self.assert_inbox_mail_data(response_data[0], recipient_emails[0])
        self.assert_inbox_mail_data(response_data[1], recipient_emails[1])

    def test_exclude_delete_mails(self):
        recipient_email = MailRecipient.objects.filter(recipient=self.recipient_2).order_by('-mail__created').first()
        recipient_email.deleted_by_recipient = True
        recipient_email.save()

        self.client.force_login(self.recipient_2)
        response = self.client.get(self.path)

        response_data = response.json()

        recipient_email_active = MailRecipient.objects.filter(recipient=self.recipient_2,
                                                              deleted_by_recipient=False).first()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_data), 1)
        self.assert_inbox_mail_data(response_data[0], recipient_email_active)

    def test_without_auth(self):
        response = self.client.get(self.path)

        response_data = response.json()

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response_data, {'detail': 'Учетные данные не были предоставлены.'})


class DeleteInboxMailTestCase(ApiTestCase):
    path = '/inbox/{}/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')

        self.mail = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail.recipients.set([self.recipient_1])

    def test_success(self):
        self.client.force_login(self.recipient_1)
        response = self.client.delete(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertTrue(mail_recipient.deleted_by_recipient)

    def test_mail_other_user(self):
        self.client.force_login(self.sender)
        response = self.client.delete(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(mail_recipient.deleted_by_recipient)

    def test_without_auth(self):
        response = self.client.delete(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertFalse(mail_recipient.deleted_by_recipient)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})

    def test_mail_not_exist(self):
        deleted_pk = self.mail.pk
        self.mail.delete()
        self.client.force_login(self.sender)
        response = self.client.delete(self.path.format(deleted_pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})


class DetailInboxMailTestCase(ApiTestCase):
    path = '/inbox/{}/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')
        self.recipient_2 = User.objects.get(email='user3@email.com')

        self.mail = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail.recipients.set([self.recipient_1])

    def test_success(self):
        self.client.force_login(self.recipient_1)
        response = self.client.get(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_inbox_mail_data(response.json(), mail_recipient)

    def test_mail_other_user(self):
        self.client.force_login(self.sender)
        response = self.client.get(self.path.format(self.mail.pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})

    def test_without_auth(self):
        response = self.client.get(self.path.format(self.mail.pk))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})

    def test_mail_not_exist(self):
        deleted_pk = self.mail.pk
        self.client.force_login(self.sender)
        response = self.client.get(self.path.format(deleted_pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})


class MarkAsReadInboxMailTestCase(ApiTestCase):
    path = '/inbox/{}/mark-as-read/'

    def setUp(self):
        self.sender = User.objects.get(email='user1@email.com')
        self.recipient_1 = User.objects.get(email='user2@email.com')

        self.mail = Mail.objects.create(title='title_1', text='text_1', sender=self.sender)
        self.mail.recipients.set([self.recipient_1])

    def test_success(self):
        self.client.force_login(self.recipient_1)
        response = self.client.post(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(mail_recipient.is_read)

    def test_mail_other_user(self):
        self.client.force_login(self.sender)
        response = self.client.post(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertFalse(mail_recipient.is_read)

    def test_without_auth(self):
        response = self.client.post(self.path.format(self.mail.pk))

        mail_recipient = MailRecipient.objects.get(recipient=self.recipient_1)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertFalse(mail_recipient.is_read)
        self.assertEqual(response.json(), {'detail': 'Учетные данные не были предоставлены.'})

    def test_mail_not_exist(self):
        deleted_pk = self.mail.pk
        self.mail.delete()
        self.client.force_login(self.sender)
        response = self.client.post(self.path.format(deleted_pk))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json(), {'detail': 'Не найдено.'})
