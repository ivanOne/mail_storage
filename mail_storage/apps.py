from django.apps import AppConfig


class MailStorageConfig(AppConfig):
    name = 'mail_storage'
